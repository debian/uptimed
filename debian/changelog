uptimed (1:0.4.6-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Update debconf template translations
    - Spanish translation.
      Thanks to Camaleón (Closes: #1029027)
    - Romanian translation.
      Thanks to Remus-Gabriel Chelu (Closes: #1090927)

 -- Helge Kreutzmann <debian@helgefjell.de>  Tue, 07 Jan 2025 20:42:02 +0100

uptimed (1:0.4.6-3) unstable; urgency=medium

  * Run fixup-duplicate-uprecords only if /var/spool/uptimed/records (or
    .old) exists. Fixes initial installation. Thanks Piuparts!

 -- Axel Beckert <abe@debian.org>  Mon, 07 Feb 2022 03:58:19 +0100

uptimed (1:0.4.6-2) unstable; urgency=low

  * Brown paper bag source only upload.

 -- Axel Beckert <abe@debian.org>  Sun, 16 Jan 2022 00:19:56 +0100

uptimed (1:0.4.6-1) unstable; urgency=low

  * debian/watch: Fix "uscan die: filenamemangle failed" error.
  * Import new upstream release 0.4.6.
    + Fixes uprecords issue "Shows duplicated entry and impossible
      values" which seems due to leap seconds. (Finally closes: #654830)
  * Add a script to clean up broken duplicate entries due to #654830
    (i.e. caused by leap seconds) and run that script at upgrade
    time. (The script does make database backups.)
  * Rename all debian/uptimed.<something> debhelper files to
    debian/<something> for easier maintenance.
  * debian/copyright:
    + Update copyright years and add myself.
    + Consequently order the copyright owners by first year ascending.
    + Consequently drop commata between years and names.
    + Remove superfluous second blank line between stanzas.

 -- Axel Beckert <abe@debian.org>  Sun, 09 Jan 2022 20:04:18 +0100

uptimed (1:0.4.5-1) unstable; urgency=medium

  * New upstream maintenance releases with only non-linux fixes.
  * Declare compliance with Debian Policy 4.6.0. (No changes required.)
  * debian/watch: follow pure git tags instead of Github-minted releases.

 -- Axel Beckert <abe@debian.org>  Thu, 04 Nov 2021 02:10:45 +0100

uptimed (1:0.4.3-1) unstable; urgency=medium

  [ Axel Beckert ]
  * Import new upstream release 0.4.3.
  * Declare compliance with Debian Policy 4.5.1. (No changes required.)
  * Bump debhelper-compat level to 13.
  * Bump debian/watch version from 3 to 4. Thanks Lintian!
  * Rename lintian overrides as needed.
  * Declare all current patches as being debian-specific.

  [ Pino Toscano ]
  * Don't ship .la files as per Debian Policy §10.2. (Closes: #947835)

 -- Axel Beckert <abe@debian.org>  Thu, 31 Dec 2020 01:11:23 +0100

uptimed (1:0.4.2-1) unstable; urgency=medium

  * Import new upstream release 0.4.2.
  * Declare compliance with Debian Policy 4.4.0. (No changes needed.)
  * Bump debhelper compatibility level to 12.
    + Build-depend on "debhelper-compat (= 12)" to replace debian/compat.

 -- Axel Beckert <abe@debian.org>  Sun, 08 Sep 2019 18:34:47 +0200

uptimed (1:0.4.1-1) unstable; urgency=medium

  * Adopt the package. (Closes: #915816)
  * Fix Vcs-Browser header.
  * Update short description to fit the new one-package layout.
  * Set "Rules-Requires-Root: no".
  * Add a DEP-12 debian/upstream/metadata file.
  * Create debian/clean to clean up leftover debian/uptimed.conf.5 file.
  * Clean up debian/rules:
    + Don't run dh_clean in build target, use debian/clean instead.
    + Call onsgmls inside the debian directory. Calling mv no more needed.
    + Don't override dh_install for "make install", but dh_auto_install.
    + Actually use dh_auto_install instead of "$(MAKE) install" with
      Debian's default parameters.
    + Shorten some multiline commands using a variable for a common path.
    + Use plain "cp" instead of "install -D -m 644" since directory is
      already created via dh_installdirs.
  * Fix debian/gbp.conf:
    + Declare actually used upstream branch (master).
    + Drop what is default anyway (compression = xz).
    + Don't use the same tag syntax for Debian as upstream does, i.e. drop
      the non-standard upstream tag syntax declaration.
  * Import upstream version 0.4.1.
    + Switches from utmp.h to utmpx.h.
    + Documentation updates
    + Drop spelling-fixes.patch, applied upstream.
  * Drop redundant entries from debian/uptimed.{install,manpages,dirs}.
  * Declare compliance with Debian Policy 4.3.0. (No changes needed.)
  * Apply "wrap-and-sort -a".

 -- Axel Beckert <abe@debian.org>  Tue, 19 Feb 2019 22:25:52 +0100

uptimed (1:0.4.0+git20150923.6b22106-2) unstable; urgency=medium

  * Remove the cgi package
  * Remove the shared library package
  * Remove the dev package
  * Run wrap-and-sort
  * Remove dependency on lsb-base
  * Move packaging vcs to salsa.debian.org
  * Use debhelper 11
  * Use secure uri for copyright specification
  * Update copyright years
  * [Debconf translation updates]
    - Portuguese (Traduz - DebianPT). (Closes: #874612)
    - Brazilian Portuguese (Adriano Rafael Gomes). (Closes: #883545)
  * Standards changes
  * Bump Standards-Version to 4.1.3.
  * Remove dh-systemd from build dependencies
  * Change priority from extra to optional
  * Lintian fixes
  * Remove trailing whitespaces on debian/changelog

 -- gustavo panizzo <gfa@zumbi.com.ar>  Sat, 03 Feb 2018 08:32:11 +0000

uptimed (1:0.4.0+git20150923.6b22106-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Update Brazilian Portuguese debconf templates translation.
    Thanks to Adriano Rafael Gomes (Closes: #770454)
  * Update Dutch debconf templates translation.
    Thanks to Frans Spiesschaert (Closes: #841654)
  * Build-Depends on opensp instead of obsolete sp (Closes: #845448)

 -- Dr. Tobias Quathamer <toddy@debian.org>  Thu, 24 Nov 2016 16:32:47 +0100

uptimed (1:0.4.0+git20150923.6b22106-1) unstable; urgency=medium

  * New maintainer, thanks Thibaut Varene for your previous
    work (Closes: #830765)
  * Packaging is now maintained in collab-maint using a git repo
  * Add Vcs urls
  * Packaging an snapshot from upstream
  * Change dh compat to level 10.
  * More modern debian/rules
  * Bump Standards-Version to 3.9.8. No changes were needed
  * Change watch file to look at github
  * Add pristine-tar to git repo
  * Drop the Replaces/Conflicts with libuptimed, gone since Etch
  * Fix an spelling mistake on uprecords command.
  * Update d/copyright
    - change to DEP5 format
    - update years
    - add Lennart Poettering (src/sd-daemon.h
      src/sd-daemon.c)
    - add Free Software Foundation, Inc (src/getopt.c src/getopt.h)
  * Update Homepage field on debian/control (Closes: #806456)
  * Handle missing /etc/uptimed.conf (Closes: #680419)
  * Match the location of the $PIDFILE in the init script and the daemon
    configuration (Closes: #336922) (LP: #482629)
  * Debconf warns about data loss if MAX_RECORDS is reduced (Closes: #573232)
  * Change the default interval to save the database from 300 to 3600 seconds,
    this will save many disks writes, it is recommended to reduce this interval
    if the system crashes on intervals smaller than an hour.
  * uptimed.conf(5) man page updated
    - to reflect above changes
    - build from sgml
  * Override unused-debconf-template lintian warnings, the templates are
    in use but lintian doesn't detect them.
  * Remove perl dependency on libuptimed0 and libuptimed-dev, perl-base is
    enough
  * Simplify uptimed init.d script
  * Restart unconditionally on uptimed postinst
  * Create /run/uptimed via a tmpfile on systemd systems
  * Update uptimed's service file, to start after the time is in sync,
    uptime may record wrong values if it is started before the time
    is correct. On systems without an RTC it is recommended to use
    systemd-timesyncd
  * Update uptimed's service to provide uptimed's documentation
  * [Debconf translation updates]
    - Russian (Yuri Kozlov). (Closes: #839797)
    - Swedish (Martin Bagge). (Closes: #839828)
    - Czech (Miroslav Kure). (Closes: #839889)
    - Danish (Joe Dalton). (Closes: #839944)
    - Polish (Mirosław Gabruś).
    - Italian (Luca Monducci). (Closes: #840126)
    - French (Julien Patriarca). (Closes: #841054)
    - German (Holger Wansing).

 -- gustavo panizzo <gfa@zumbi.com.ar>  Tue, 18 Oct 2016 15:58:19 +0800

uptimed (1:0.3.17-4) unstable; urgency=low

  * Convert to dh-systemd (Closes: #716818)
  * Updated Japanese translation (Closes: #712923)
  * Ack NMU (Closes: #677927)
  * Fix uprecords-cgi postinst (Closes: #734713)
  * (Config handling still needs love)

 -- Thibaut VARENE <varenet@debian.org>  Sun, 26 Jan 2014 14:58:19 +0100

uptimed (1:0.3.17-3.1) unstable; urgency=low

  * Non-maintainer upload with maintainer's agreement.
  * Debconf templates and debian/control reviewed by the debian-l10n-
    english team as part of the Smith review project. Closes: #674860
  * [Debconf translation updates]
    - Danish (Joe Hansen).  Closes: #675518
    - Russian (Yuri Kozlov).  Closes: #675565
    - Czech (Miroslav Kure).  Closes: #675580
    - Dutch; (Jeroen Schot).  Closes: #674852
    - Spanish; (Rudy Godoy).  Closes: #676138
    - French (Christian Perrier).  Closes: #676153
    - Italian (Luca Monducci).  Closes: #676278
    - Portuguese (Pedro Ribeiro).  Closes: #676284
    - Polish (Michał Kułach).  Closes: #676513
    - Polish (Michał Kułach).  Closes: #676513
    - Swedish (Martin Bagge / brother).  Closes: #676557
    - German (Holger Wansing).  Closes: #676788
    - Slovak (Ivan Masár).  Closes: #677283

 -- Christian Perrier <bubulle@debian.org>  Sun, 17 Jun 2012 21:26:46 +0200

uptimed (1:0.3.17-3) unstable; urgency=low

  * Fix non-interactive setup, patch from Andreas Beckmann (Closes: #674624)

 -- Thibaut VARENE <varenet@debian.org>  Sun, 27 May 2012 22:46:42 -0700

uptimed (1:0.3.17-2) unstable; urgency=low

  * Fix for systemd .service file from Shawn Landden

 -- Thibaut VARENE <varenet@debian.org>  Fri, 25 May 2012 11:34:51 -0700

uptimed (1:0.3.17-1) unstable; urgency=low

  * New upstream release (Closes: #648948, #650109)
  * Switch to dpkg-source 3.0 (quilt) format
  * Move PID to /run and provide systemd .service file (Closes: #657922)
  * Use maintscript support, patch from Colin Watson (Closes: #659797)
  * Add status in init script, patch from Peter Eisentraut (Closes: #645504)
  * Enabled hardened build
  * Fixed some lintian warnings

 -- Thibaut VARENE <varenet@debian.org>  Fri, 25 May 2012 17:47:05 -0700

uptimed (1:0.3.16-3.2) unstable; urgency=low

  * Non-maintainer upload with maintainer approval.
  * Fix uptimed purge failure (Closes: #636596)
  * Change default recipient of milestone mails to root (Closes: #629322)
  * Fix uptimed debconf backup/cancel handling (Closes: #500497)
  * Fix lintian error weak-library-dev-dependency
  * Fix lintian warning for uprecords-cgi short description
  * Don't install uptimed.sh in rcS
  * Remove old uptimed.sh conffiles on upgrade
  * Add quilt patch system
  * Add patch to avoid the need to create a bootid file
    + no-bootid-file-on-linux.patch
    (The above four changes Closes: #626687)
  * Convert direct source edits to patches
    + uprecords-cgi-header-path.patch
    + uprecords-cgi-header-text-color.patch
  * Revert direct source edits since they are now patches
    - also remove src/test
  * Remove debian/foo
  * Add database corruption detection/recovery patch (Closes: #515653)
    - Authored by Thibaut VARENE, tested by Martin Steigerwald

 -- Andreas Henriksson <andreas@fatal.se>  Sat, 27 Aug 2011 08:35:45 +0200

uptimed (1:0.3.16-3.1) unstable; urgency=low

  * Non-maintainer upload.
  * With maintainer's agreement, remove Daniel Gubser from Uploaders
  * Some trivial lintian fixes:
    - Explicitly refer to GPL-2 license file in debian/copyright
    - Insert copyright information in debian/copyright
    - Add ${misc:Depends} to binary packages dependencies to
      properly cope with dependencies triggerred by debhelper
      utilities
    - Bump debhelper compatibility to 7. This also
      triggers the suggested replacement of "dh_clean -k" by
      dh_prep and increasing the build dependency on debhelper
    - Split Choices in debconf templates
  * Fix pending l10n issues. Debconf translations:
    - Russian (Yuri Kozlov).  Closes: #531206
    - Vietnamese (Clytie Siddall).  Closes: #569649
    - Slovak (Ivan Masár).  Closes: #570288
    - Fill in header field for German translation with safe
      values so that calls for translation updates may work
      While at it, convert the file to UTF-8.
    - While at it, convert ALL translations to UTF-8.

 -- Christian Perrier <bubulle@debian.org>  Sat, 20 Feb 2010 14:56:58 +0100

uptimed (1:0.3.16-3) unstable; urgency=low

  * Increase default db write time to 10 minutes
  * Use dh_makeshlibs -V

 -- Thibaut VARENE <varenet@debian.org>  Mon, 16 Feb 2009 20:01:42 +0100

uptimed (1:0.3.16-2) unstable; urgency=low

  * Make uptimed depend on up to date libuptimed (Closes: #514726)

 -- Thibaut VARENE <varenet@debian.org>  Tue, 10 Feb 2009 13:38:53 +0100

uptimed (1:0.3.16-1) unstable; urgency=low

  * New upstream release
  * Use a backup database (Closes: #482643,#504714)
  * Reformat descriptions (Closes: #480352)
  * [INTL:ja] Japanese po-debconf template translation (Closes: #512871)

 -- Thibaut VARENE <varenet@debian.org>  Mon, 09 Feb 2009 15:29:23 +0100

uptimed (1:0.3.12-2) unstable; urgency=low

  * Closes: #460058: add LSB formatted dependency info in init.d script
    (patch from Petter Reinholdtsen)

 -- Thibaut VARENE <varenet@debian.org>  Sun, 13 Jan 2008 17:50:25 +0100

uptimed (1:0.3.12-1) unstable; urgency=low

  * New upstream release
  * uprecords shows more stats

 -- Thibaut VARENE <varenet@debian.org>  Thu, 06 Dec 2007 11:57:59 +0100

uptimed (1:0.3.11-1) unstable; urgency=low

  * New upstream release
  * [INTL:it] Italian debconf templates translation (Closes: #422916)

 -- Thibaut VARENE <varenet@debian.org>  Wed, 09 May 2007 14:28:51 +0200

uptimed (1:0.3.10-3) unstable; urgency=medium

  * [INTL:gl] Galician translation (Closes: #412642)
  * Change Maintainer/Uploaders in accordance with Daniel

 -- Thibaut VARENE <varenet@debian.org>  Mon,  5 Mar 2007 14:55:25 +0100

uptimed (1:0.3.10-2) unstable; urgency=medium

  * [INTL:pt] Portuguese translation (Closes: #412038)
  * Fix not-binnmuable-all-depends-any uprecords-cgi -> uptimed

 -- Thibaut VARENE <varenet@debian.org>  Fri, 23 Feb 2007 04:04:04 +0100

uptimed (1:0.3.10-1) unstable; urgency=medium

  * Sync with upstream versionning
  * No longer truncates kernel version strings (Closes: #192395)

 -- Thibaut VARENE <varenet@debian.org>  Tue, 20 Feb 2007 03:11:39 +0100

uptimed (1:0.3.8-3) unstable; urgency=low

  * [INTL:es] Spanish po-debconf translation (Closes: #404491)
  * Small patch to fix end of line ANSI codes (Closes: #256968)

 -- Thibaut VARENE <varenet@debian.org>  Mon,  1 Jan 2007 02:38:33 +0100

uptimed (1:0.3.8-2) unstable; urgency=low

  * Fix Makefile.in (missing $(DESTDIR))

 -- Thibaut VARENE <varenet@debian.org>  Sun,  3 Dec 2006 14:32:30 +0100

uptimed (1:0.3.8-1) unstable; urgency=low

  * New upstream release
  * uprecords.cgi now uses default files in /etc/uprecords-cgi (Closes: #383516)
  * Acknowledgement for NMU (Closes: #389834)

 -- Thibaut VARENE <varenet@debian.org>  Fri, 24 Nov 2006 23:47:32 +0100

uptimed (1:0.3.7-3.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Replace libuptimed for smooth sarge to etch upgrades. (Closes: #388314)

 -- Mohammed Adnène Trojette <adn+deb@diwi.org>  Thu, 28 Sep 2006 02:17:57 +0200

uptimed (1:0.3.7-3) unstable; urgency=low

  * fix broken dependency libuptimed-dev -> libuptimed

 -- Thibaut VARENE <varenet@debian.org>  Thu,  6 Jul 2006 04:56:19 +0200

uptimed (1:0.3.7-2) unstable; urgency=low

  * background color patch back in

 -- Thibaut VARENE <varenet@debian.org>  Sat, 24 Jun 2006 01:12:28 +0200

uptimed (1:0.3.7-1) unstable; urgency=low

  * [INTL:sv] Swedish debconf templates translation (Closes: #351313)
  * [INTL:nl] New dutch po-debconf translation (Closes: #364918)
  * upstream maintainership changed, new version (Closes: #350953)
  * Cleaned up debian/* - added watchfile
  * Adding myself to Uploaders

 -- Thibaut VARENE <varenet@debian.org>  Mon,  5 Jun 2006 17:22:32 +0200

uptimed (1:0.3.3-8) unstable; urgency=low

  * changed dependency to | debconf-2.0 (Closes: #332134)
  * added Vietnamese translation (Closes: #324066)

 -- Daniel Gubser <guterm@debian.org>  Fri, 14 Oct 2005 07:28:41 +0200

uptimed (1:0.3.3-7) unstable; urgency=low

  * downgraded automake to 1.4 for building (Closes: #316585)

 -- Daniel Gubser <guterm@debian.org>  Wed,  6 Jul 2005 08:11:49 +0200

uptimed (1:0.3.3-6) unstable; urgency=low

  * corrected disparities in section for libuptimed and libuptimed-dev

 -- Daniel Gubser <guterm@debian.org>  Tue, 28 Jun 2005 15:25:23 +0200

uptimed (1:0.3.3-5) unstable; urgency=low

  * added Portuguese translation to PO files (Closes: #307124)

 -- Daniel Gubser <guterm@debian.org>  Tue, 28 Jun 2005 10:20:40 +0200

uptimed (1:0.3.3-4) unstable; urgency=low

  * changed depency to libtool (Closes: #307487)
  * sends E-Mails again (Closes: #274635, #280701)
  * changed links to active Homepage (Closes: #300841)
  * added translation for Czech translation (Closes: #298211)
  * updated automake version to 1.9

 -- Daniel Gubser <guterm@debian.org>  Wed, 22 Jun 2005 06:54:52 +0200

uptimed (1:0.3.3-3) unstable; urgency=low

  * use the right version scheme (non NMU's)
  * ACK closes bugs (Closes: #276977, #143434, #209801, #167486, #167486)
  * and more bugs (Closes: #184910, #207425, #220819, #224702,	#224818)
  * and (Closes: #166324, #221901, #259477)

 -- Daniel Gubser <guterm@debian.org>  Wed,  6 Apr 2005 06:11:14 +0200

uptimed (1:0.3.3-2.4) unstable; urgency=low

  * corrected the Depends to Build-Depends (Closes: #300790)
  * changed the background-color for uptimed-cgi (Closes: #300942)

 -- Daniel Gubser <guterm@debian.org>  Wed, 23 Mar 2005 06:30:44 +0100

uptimed (1:0.3.3-2.3) unstable; urgency=low

  * added Build-Depends to automake1.4 + libtool1.4 (Closes: #300790)

 -- Daniel Gubser <guterm@debian.org>  Tue, 22 Mar 2005 10:38:48 +0100

uptimed (1:0.3.3-2.2) unstable; urgency=low

  * NMU.
  * Install urec.h as /usr/include/uptimed.h instead.

 -- Robert Millan <rmh@debian.org>  Mon,  7 Feb 2005 16:44:46 +0100

uptimed (1:0.3.3-2.1) unstable; urgency=low

  * NMU (with maintainer's permission).
  * Add libuptimed-dev package with headers and static objects.  Thanks
    Robin Elfrink.  (Closes: #259477)
  * Update libtool and config.{guess,sub}.

 -- Robert Millan <rmh@debian.org>  Mon,  7 Feb 2005 15:48:00 +0100

uptimed (1:0.3.3-2) unstable; urgency=low

  * added patch to Enable kernel-specific read_uptime for GNU/k*BSD
    (Closes: #259472)
  * Changed source for correct location of header & footer for
    uprecords-cgi (Closes: #281262)
  * Corrected uprecords.postinst (Closes: #255501)

 -- Daniel Gubser <guterm@debian.org>  Fri,  4 Feb 2005 12:02:59 +0100

uptimed (1:0.3.3-1.1) unstable; urgency=low

  * NMU
  * Fix dependency (Closes: #276977)
  * libuptimed does not need perl

 -- Julien Danjou <acid@debian.org>  Sun, 31 Oct 2004 14:06:15 +0100

uptimed (1:0.3.3-1) unstable; urgency=low

  * New upstream release
  * tighter dependency to libuptimed (Closes: #256943) and (Closes: #254677)

 -- Daniel Gubser <guterm@debian.org>  Thu, 24 Jun 2004 07:01:52 +0200

uptimed (1:0.3.2-1) unstable; urgency=low

  * New upstream release
  * added catalan debconf templates (Closes: #248757)

 -- Daniel Gubser <guterm@debian.org>  Wed,  2 Jun 2004 06:21:21 +0200

uptimed (1:0.3.1-2) unstable; urgency=low

  * updating the German debconf-l10n (Closes: #244545)
  * added Danish debconf translation (Closes: #237107)
  * corrected small type in uptimed.conf.5 (Closes: #234330)
  * added FILES section in uptimed.5 (Closes: #238113)
  * hardcoded location to uprecord.conf in src/uprecords.c
    so uprecords-cgi is displayed as wanted (Closes: #243523)
  * Fixed package description of uprecords-cgi (Closes: #209807)

 -- Daniel Gubser <guterm@debian.org>  Mon, 19 Apr 2004 07:36:49 +0200

uptimed (1:0.3.1-1) unstable; urgency=low

  * New upstream release
  * patched source for handling long command line arguments better for pidfile
      and email settings (Closes: #220819)
  * Fixed package description of libuptimed (Closes: #209801)
  * Fixes package description of uprecords-cgi (Closes: #167486)
  * Booting doesn't use cron.d anymore (Closes: #184910)
  * Fixed script for debconf-questions (Closes: #224702)
  * Fixed ownership of /var/spool/uptimed/ files (Closes: #224818)
  * Changed read-right for /etc/uptimed.conf for '-M' option (Closes: #227046)
  * Added man page for uptimed.conf from Helge Kreutzmann (Closes: #166324)
  * Addes patch for templates for debconf (Closes: #221901)
  * Bugfix from upstream for out-of-space servers or system crashes
    (Closes: #207424) and (Closes: #207425)

 -- Daniel Gubser <guterm@debian.org>  Mon, 19 Jan 2004 06:11:39 +0100

uptimed (1:0.3.0-1) unstable; urgency=low

  * Non-Maintainer upload
  * New upstream release (Closes: #184007)
  * convert to non-native version (Closes: #191665)
  * changes subject line by upstream (Closes: #159307)
  * switched to gettext for the debconf templates (Closes: #199343)
  * added french translation of the debconf templates (Closes: #201344)
  * changes debconf entry in postinst from MAX_ENTRIES
    to LOG_MAXIMUM_ENTRIES (Closes: #161112)
  * runs as user daemon (not root) (Closes: #153798)

 -- Daniel Gubser <daniel.gubser@gutreu.ch>  Thu, 16 Oct 2003 16:43:19 +0200

uptimed (1:0.2.0-0.2) unstable; urgency=low

  * convert to non-native version

 -- Daniel Gubser <daniel.gubser@gutreu.ch>  Thu, 16 Oct 2003 16:21:35 +0200

uptimed (1:0.2.0-0.1) unstable; urgency=low

  * Non-maintainer upload
  * New upstream version
  * Fix Hurd compilation problem

 -- Julien Danjou <acid@debian.org>  Wed, 21 Aug 2002 00:55:43 +0200

uptimed (1:0.1.7-2) unstable; urgency=low

  * Added a missing $DESTDIR (Closes: #135023)
  * Fixed uptimed.sh to use -b instead of -boot which no longer works
    (Closes: #135027)

 -- Simon Richter <sjr@debian.org>  Thu, 21 Feb 2002 23:22:05 +0100

uptimed (1:0.1.7-1) unstable; urgency=low

  * New upstream version (Closes: #107492)
  * Fixed a bug in uprecords-cgi.postinst which could leave the config
    files unreadable for the webserver (Closes: #100480, #109562)

 -- Simon Richter <sjr@debian.org>  Tue, 19 Feb 2002 21:26:35 +0100

uptimed (1:0.1.6-7) unstable; urgency=low

  * Replaced a "source" that slipped through with a dot (Closes:
    #108110).

 -- Simon Richter <sjr@debian.org>  Thu,  9 Aug 2001 07:07:49 +0200

uptimed (1:0.1.6-6) unstable; urgency=low

  * Fixed a stupid typo that made uptimed send out emails although
    configured otherwise (Closes: #106991).

 -- Simon Richter <sjr@debian.org>  Wed,  1 Aug 2001 06:14:50 +0200

uptimed (1:0.1.6-5) unstable; urgency=low

  * Applied patch from Michal Politowski which fixes a few annoyances
    (Closes: #102228).
  * Added THRESHOLD/-t option (useful if you don't want each reboot to
    show up but only serious records) (Closes: #100596)
  * Added option to select which mails to send, for milestones only,
    records only or both (Closes: #100592)
  * Added PIDFILE/-p option and pidfile creation (Closes: #102508).
  * Removed all CRs from the mailing (Closes: #104049).

 -- Simon Richter <sjr@debian.org>  Thu, 26 Jul 2001 02:27:56 +0200

uptimed (1:0.1.6-4) unstable; urgency=low

  * Re-did the packaging (Closes: #95465)
  * Moved to debhelper 3 and debconf
  * Added a missing NULL init (Closes: #78279)

 -- Simon Richter <sjr@debian.org>  Thu, 21 June 2001 23:03:39 +0100

uptimed (1:0.1.6-3) unstable; urgency=low

  * Updated the copyright file

 -- Simon Richter <richtesi@fs.tum.de>  Wed, 21 Mar 2001 12:18:21 +0100

uptimed (1:0.1.6-2) unstable; urgency=low

  * Added a crontab entry to have uptimed generate a new bootid at startup
    (Closes: #90260)
  #* Moved from manually installing the manpages to dh_installmanpages
  * Added a missing call to dh_installinit (which hasn't caused any problems,
    but...)
  * Removed obsolete dh_suidregister and dh_testversion calls
  * Removed upgrade instructions from the package

 -- Simon Richter <Simon.Richter@phobos.fs.tum.de>  Mon, 19 Mar 2001 19:26:29 +0100

uptimed (1:0.1.6-1) unstable; urgency=low

  * New upstream release
  * Took over maintainership

 -- Simon Richter <Simon.Richter@phobos.fs.tum.de>  Mon, 12 Feb 2001 21:47:36 +0100

uptimed (1:0.1.5-5) unstable; urgency=low

  * Updated Standards-Version to 3.1.2
  * /usr/lib/menu/uptimed said uprecord instead of uprecords, this bug was
    spotted by Daniel Bayer <daniel.bayer@knuut.de> Closes: Bug#71094

 -- Edward Betts <edward@debian.org>  Fri, 15 Sep 2000 15:38:37 -0600

uptimed (1:0.1.5-4) unstable; urgency=low

  * Fiddled with debian/postinst to create bootid on fresh install
    Closes: Bug#67611.

 -- Edward Betts <edward@debian.org>  Sun, 23 Jul 2000 12:12:04 +0100

uptimed (1:0.1.5-3) unstable; urgency=low

  * added two manpages uprecords.1 and updated.8, written and provided by
    Alan Ford <alan@whirlnet.co.uk>. Thanks Alan! Closes: Bug#.
  * debian/control: split package into uptimed and uprecords-cgi.
  * debian/rules: modified to handle package split.
  * debian/postrm: modified to only run update-rc.d on purge.

 -- Edward Betts <edward@debian.org>  Tue, 18 Apr 2000 16:51:19 +0100

uptimed (1:0.1.5-2) unstable; urgency=low

  * debian/uptimed.sh: boot script added Closes: Bug#62047.
  * debian/init: removed boot handling.

 -- Edward Betts <edward@debian.org>  Mon, 17 Apr 2000 16:44:37 +0100

uptimed (1:0.1.5-1) unstable; urgency=low

  * New upstream release.
  * Closes: Bug#61359.

 -- Edward Betts <edward@debian.org>  Thu, 30 Mar 2000 14:32:33 +0100

uptimed (1:0.1.4-2) unstable; urgency=low

  * Fix problems upgrading from older package. (Closes: Bug#61240)
    Thanks to Tuomas Jormola <tj@sgic.fi> and bonnaud@iut2.upmf-grenoble.fr
  * Added call to dh_undocumented for both binaries.
  * I have this package really sorted now. Testing completed:
        - Installing from fresh.
        - Upgrading from frozen version 0.03
        - Upgrading from unstable version 0.1.4
        - Removing the package.

 -- Edward Betts <edward@debian.org>  Tue, 28 Mar 2000 19:52:49 +0100

uptimed (1:0.1.4-1) unstable; urgency=low

  * New maintainer.
  * New upstream release.

 -- Edward Betts <edward@debian.org>  Sat, 25 Mar 2000 16:05:12 +0000

uptimed (0.03-3) unstable; urgency=low

  * Wrote a manpage for uprecords.

 -- Robert S. Edmonds <stu@novare.net>  Wed, 17 Feb 1999 16:51:27 -0500

uptimed (0.03-2) unstable; urgency=low

  * /etc/init.d/uptimed now gives proper output.
  * Manpages now undocumented. Fixes bug #33443.
  * /etc/uptimed.log moved to /var/log/uptimed. Fixes bug #33294.

 -- Robert S. Edmonds <stu@novare.net>  Tue, 16 Feb 1999 18:02:09 -0500

uptimed (0.03-1) unstable; urgency=low

  * Initial Release.

 -- Robert S. Edmonds <stu@novare.net>  Sat,  6 Feb 1999 22:32:19 -0500
