# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the uptimed package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: uptimed\n"
"Report-Msgid-Bugs-To: uptimed@packages.debian.org\n"
"POT-Creation-Date: 2018-01-28 22:11+0800\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: string
#. Description
#: ../uptimed.templates:2001
msgid "Delay between database updates (seconds):"
msgstr ""

#. Type: string
#. Description
#: ../uptimed.templates:2001
msgid ""
"Uptimed will update its database regularly so that the uptime doesn't get "
"lost in case of a system crash. You can set how frequently this will happen "
"(use higher values if you want to avoid disk activity, for instance on a "
"laptop)."
msgstr ""

#. Type: string
#. Description
#: ../uptimed.templates:3001
msgid "Number of records that should be kept:"
msgstr ""

#. Type: string
#. Description
#: ../uptimed.templates:3001
msgid ""
"Uptimed can limit the number of records to be kept to the highest n, to keep "
"an unlimited number of records set this value to 0"
msgstr ""

#. Type: string
#. Description
#: ../uptimed.templates:3001
msgid ""
"Be aware that uptime data will be lost if the limit has been reached and/or "
"the number records is reduced."
msgstr ""

#. Type: select
#. Choices
#: ../uptimed.templates:4001
msgid "Never"
msgstr ""

#. Type: select
#. Choices
#: ../uptimed.templates:4001
msgid "Record"
msgstr ""

#. Type: select
#. Choices
#: ../uptimed.templates:4001
msgid "Milestone"
msgstr ""

#. Type: select
#. Choices
#: ../uptimed.templates:4001
msgid "Both"
msgstr ""

#. Type: select
#. Description
#: ../uptimed.templates:4002
msgid "Send mails if a milestone or record is reached:"
msgstr ""

#. Type: select
#. Description
#: ../uptimed.templates:4002
msgid ""
"Uptimed can be configured to send a mail each time a record is broken or a "
"\"milestone\" is reached. You can choose whether you:"
msgstr ""

#. Type: select
#. Description
#: ../uptimed.templates:4002
msgid ""
" * never want to receive these mails;\n"
" * want to be notified only when a record is broken;\n"
" * would like to know about milestones;\n"
" * are interested in both."
msgstr ""

#. Type: string
#. Description
#: ../uptimed.templates:5001
msgid "Uptimed email recipient:"
msgstr ""

#. Type: string
#. Description
#: ../uptimed.templates:5001
msgid ""
"Since you have chosen to be sent emails, you should specify where to send "
"these mails."
msgstr ""

#. Type: note
#. Description
#: ../uptimed.templates:6001
msgid "Milestone configuration must be done manually"
msgstr ""

#. Type: note
#. Description
#: ../uptimed.templates:6001
msgid ""
"The milestones must be configured manually in /etc/uptimed.conf. Since you "
"have chosen to receive emails for milestones you probably want to modify "
"that file."
msgstr ""
